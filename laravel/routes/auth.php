<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\PainelController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'painel',
], function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');

    // PAINEL
    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('/', [PainelController::class, 'index'])->name('painel');
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);
        // Route::resource('usuarios', UsersController::class);
        // Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
        // Route::resource('politica-de-privacidade', PoliticaDePrivacidadeController::class)->only(['index', 'update']);
        // Route::get('aceite-de-cookies', [AceiteDeCookiesController::class, 'index'])->name('aceite-de-cookies.index');
        // Route::resource('contatos', ContatosController::class)->only(['index', 'update']);
        // Route::get('contatos-recebidos/{id}/toggle', [ContatosRecebidosController::class, 'toggle'])->name('contatos-recebidos.toggle');
        // Route::resource('contatos-recebidos', ContatosRecebidosController::class)->only(['index', 'show', 'destroy']);
        // Route::resource('banners', BannersController::class);
        // Route::resource('perfil', PerfilController::class)->only(['index', 'update']);
        // Route::resource('projetos', ProjetosController::class);
        // Route::get('projetos/{projeto}/imagens/clear', [ProjetosImagensController::class, 'clear'])->name('projetos.imagens.clear');
        // Route::resource('projetos.imagens', ProjetosImagensController::class);
        // Route::resource('clippings', ClippingsController::class);
        // Route::get('clippings/{clipping}/imagens/clear', [ClippingsImagensController::class, 'clear'])->name('clippings.imagens.clear');
        // Route::resource('clippings.imagens', ClippingsImagensController::class);


        // Route::resource('grupos.servicos', ServicosController::class);
        // Route::get('trabalhe-conosco/{id}/toggle', [TrabalheConoscoController::class, 'toggle'])->name('trabalhe-conosco.toggle');
        // Route::resource('trabalhe-conosco', TrabalheConoscoController::class)->only(['index', 'show', 'destroy']);

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });
    });
});