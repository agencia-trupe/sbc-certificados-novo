import MobileToggle from "./MobileToggle";

MobileToggle();

$(window).on("load", function () {});

$(document).ready(function () {
    var urlPrevia = "";
    // var urlPrevia = "/previa-hospitalcentralleste";

    

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + urlPrevia + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
