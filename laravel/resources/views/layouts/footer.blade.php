<footer>

    <div class="center">
        <div class="rights">
                <a href="" style="text-decoration: underline;"><p>POLÍTICA DE PRIVACIDADE</p></a>
                <p class="dados"> | © {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                <p class="direitos"> - Todos os direitos reservados. </p>
        </div>
      
            <img src="{{ asset('assets/img/layout/marca-SBC-rodape.svg')}}" alt="">
        
    </div>
    
</footer>