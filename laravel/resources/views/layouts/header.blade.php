<header>
   <div class="external">
       <div class="ajuste">
           <div class="bege"></div>
            <div class="imagem">
            <div class="img"></div>
            </div>
        </div>
        <div class="superior">
            <div class="info">
                <h1>Sistema E-cards SBC</h1>
                <div class="account">
                    <div class="flex">
                        <p>Olá Fulano! [CONFIGURAÇÕES]</p> <a href="">SAIR<img src="{{ asset('assets/img/layout/x-sair.svg')}}" alt=""></a> 
                    </div>
                    <div class="search">
                        <input type="text" placeholder="buscar">
                        <img src="{{ asset('assets/img/layout/ico-lupa.svg')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="inferior">
            <nav>
                <a href="">
                    <div class="menu @if(Tools::routeIs('geral*')) active @endif">
                        VISÃO GERAL
                    </div>
                </a>
                <a href="">
                    <div class="menu @if(Tools::routeIs('turmas*')) active @endif">
                        CERTIFICADOS/TURMAS
                    </div>
                </a>
                <a href="">
                    <div class="menu @if(Tools::routeIs('instrutores*')) active @endif">
                        INSTRUTORES
                    </div>
                </a>
                <a href="">
                    <div class="menu @if(Tools::routeIs('cursos*')) active @endif">
                        CURSOS
                    </div>
                </a>
                <a href="">
                    <div class="menu @if(Tools::routeIs('alunos*')) active @endif">
                        ALUNOS
                    </div>
                </a>
            </nav>
        </div>

        <nav>
        
        </nav>
    
    </div>
   
</header>