@extends('layouts.template')

@extends('layouts.header')

@section('content')

<main>

    <div class="geral" style="display: none;">
        <!-- CERTIFICADOS EMITIDOS -->
        <div class="emitidos">
            <h1>ÚLTIMOS CERTIFICADOS EMITIDOS</h1>
            <hr>
            <div class="line">
                <p>12/12/2021</p>
                <p>Treinamento de Emergências Cardiovasculares</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p>12/12/2021</p>
                <p>Suporte Avançado de Vida Cardiovascular</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p>12/12/2021</p>
                <p>Suporte Avançado de Vida Cardiovascular</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p>12/12/2021</p>
                <p>Suporte Avançado de Vida Cardiovascular</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p>12/12/2021</p>
                <p>Suporte Avançado de Vida Cardiovascular</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p>12/12/2021</p>
                <p>Suporte Avançado de Vida Cardiovascular</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" alt=""></a>
            </div>
            <hr>
        </div>
    

        <!-- CERTIFICADOS COM VENCIMENTO PROXIMO -->
        <div class="vencimento">
            <h1>CERTIFICADOS COM VENCIMENTO PRÓXIMO</h1>
            <hr>
            <div class="line">
                <p class="sentence">Suporte Avançado de Vida Cardiovascular</p>
                <p>12/12/2021</p>                   <!-- ATENÇÃO PARA A TROCA DE ICONE AQUI -->
                <a href=""><img src="{{ asset('assets/img/layout/ico-extrair-mailing.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p class="sentence">Suporte Avançado de Vida Cardiovascular</p>
                <p>12/12/2021</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-extrair-mailing.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p class="sentence">Suporte Avançado de Vida Cardiovascular</p>
                <p>12/12/2021</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-validado.svg')}}" alt=""></a>
            </div>
            <hr>
            <div class="line">
                <p class="sentence">Suporte Avançado de Vida Cardiovascular</p>
                <p>12/12/2021</p>
                <a href=""><img src="{{ asset('assets/img/layout/ico-validado.svg')}}" alt=""></a>
            </div>
            <hr>
        </div>
    </div>


    <!-- TELA 4, 5 E 6 VENCIMENTO -->
    <div class="certificado_venc">
        
            <div class="card1">
                <h1>CERTIFICADOS COM VENCIMENTO PRÓXIMO</h1>
                <hr>
                <h4>Treinamento de Emergências Cardiovasculares</h4>
                <div class="flex">
                    <p class="label">INSTRUTOR: </p><p class="dados">Fulano de Tal da Silva</p>
                </div>
                <div class="flex">
                    <p class="label">DATA DE EMISSÃO: </p> <p class="dados">20/09/2021</p>
                </div>
                <div class="flex">
                    <p class="label">LOCAL: </p> <p class="dados">Rio de Janeiro - RJ</p>
                </div>
                <br>
                <div class="flex">
                    <p class="label">VALIDADE: </p> <p class="dados">20/03/2022</p>
                </div>
                <a href=""><div class="button">
                    EXTRAIR MAILING DE ALUNOS <img src="{{ asset('assets/img/layout/ico-extrair-mailing.svg')}}" alt="">
                </div></a>
            </div>

            <!-- TELA 4 -->
            <div class="card2" style="display: none;">
                <h3>AVISO DE VENCIMENTO</h3>
                <p>ESTE CERTIFICADO AINDA NÃO TEVE DISPARO DE AVISO DE VENCIMENTO MARCADO COMO ENVIADO</p>
                <a href=""><div class="button">
                    MARCAR COMO ENVIADO
                </div></a>
            </div>

            <!-- TELA 5 -->
            <div class="card2" style="display: none;">
                <h3>AVISO DE VENCIMENTO</h3>
                <p>ESTE CERTIFICADO AINDA NÃO TEVE DISPARO DE AVISO DE VENCIMENTO MARCADO COMO ENVIADO</p>
                <div class="flex">
                    <input type="text" placeholder="20/12/2021">
                    <button class="button2">
                        OK
                    </button>
                </div>
            </div>

            <!-- TELA 6 -->
            <div class="card3">
                <div class="thumbs"> 
                    <img src="{{ asset('assets/img/layout/ico-validado.svg')}}" alt="">
                    <h3>AVISO DE VENCIMENTO</h3>
                </div>
                <div class="flex">
                    <p class="label">AVISO ENVIADO EM: </p>
                    <p class="dados">20/12/2021</p>
                </div>
                <a href=""><div class="button">
                    MARCAR COMO ENVIADO
                </div></a>
            </div>
        </div>

    
</main>



@endsection