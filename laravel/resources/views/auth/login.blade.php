<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/font/bootstrap-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel.css') }}">

</head>

<body class="painel-login">

    <div class="col-md-4 col-sm-6 col-xs-10 login">

        <h3 class="mb-5">
            {{ config('app.name') }}
            <small class="h6 lead mb-0" style="color:#b4bcc2;">Painel Administrativo</small>
        </h3>

        {!! Form::open(['route' => 'login']) !!}

        <!-- Email Address -->
        <div class="input-group">
            <span class="input-group-text">
                <i class="bi bi-person-fill"></i>
            </span>
            {!! Form::text('email', null, [
            'class' => 'form-control',
            'placeholder' => 'usuário ou e-mail',
            'autofocus' => true,
            'required' => true
            ]) !!}
        </div>

        <!-- Password -->
        <div class="input-group">
            <span class="input-group-text">
                <i class="bi bi-lock-fill"></i>
            </span>
            {!! Form::password('password', [
            'class' => 'form-control',
            'placeholder' => 'senha',
            'required' => true
            ]) !!}
        </div>

        <!-- Remember Me -->
        <div class="block mt-3">
            <label for="remember_me" class="inline-flex items-center">
                <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                <span class="ml-2 text-sm text-gray-600">Lembrar de mim</span>
            </label>

            <a href="{{ route('password.request') }}" class="nav-link py-0" style="float:right;font-size:14px;">Esqueci minha senha</a>
        </div>

        {!! Form::submit('Login', ['class' => 'btn btn-success col-12', 'style' => 'margin-top:20px;height:45px']) !!}
        {!! Form::close() !!}

        <!-- Session Status -->
        <x-auth-session-status class="flash flash-sucesso mt-2" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="flash flash-erro mt-2" :errors="$errors" />
    </div>
</body>